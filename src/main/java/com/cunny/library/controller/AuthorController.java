package com.cunny.library.controller;

import com.cunny.library.model.Author;
import com.cunny.library.model.Book;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController("/authors")
public class AuthorController {

    @GetMapping("/author/{id}")
    public ResponseEntity<?> getBookById(@PathVariable("id") Long id) {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/author")
    public ResponseEntity<?> addBook(@RequestBody Author book) {
        return ResponseEntity.ok().build();
    }
}
