package com.cunny.library.controller;

import com.cunny.library.dto.BookDTO;
import com.cunny.library.model.Book;
import com.cunny.library.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping("/book/{name}")
    public List<Book> getBookById(@PathVariable("name") String name) {
        return bookService.getBookByName(name);
    }

    @PostMapping("/book")
    public ResponseEntity<?> addBook(@RequestBody BookDTO book) {
        bookService.createBook(book);
        return ResponseEntity.ok().build();
    }
}
