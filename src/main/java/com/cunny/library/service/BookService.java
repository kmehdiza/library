package com.cunny.library.service;

import com.cunny.library.dto.BookDTO;
import com.cunny.library.model.Book;
import com.cunny.library.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    public void createBook(BookDTO bookDto) {
        Book book = convertDTOToBook(bookDto);
        bookRepository.save(book);
    }

    public List<Book> getBookByName(String bookName) {
        return bookRepository.findByNameContainsIgnoreCase(bookName);
    }

    private Book convertDTOToBook(BookDTO bookDTO) {
        return Book
                .builder()
                .name(bookDTO.getName())
                .authorName(bookDTO.getAuthorName())
                .build();
    }
}
